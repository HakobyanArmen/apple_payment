//
//  HasDisposeBag.swift
//  ApplePayment
//
//  Created by Armen Hakobyan on 30/10/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import RxSwift

fileprivate var disposeBagKey = "disposeBagKey"

protocol HasDisposeBag: class, AssociatedObjectStore {
    var disposeBag: DisposeBag { get }
}

extension HasDisposeBag {
    var disposeBag: DisposeBag {
        get {
            return self.associatedObject(forKey: &disposeBagKey, default: DisposeBag())
        }
        set {
            self.setAssociatedObject(newValue, forKey: &disposeBagKey)
        }
    }
}
