//
//  ProductsViewController.swift
//  ApplePayment
//
//  Created by Armen Hakobyan on 30/10/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import StoreKit

class ProductsViewController: StorableViewController {
    
    override var serviceType: ServiceType { return .delegative }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Dependency.shared.initialize()

        //product service input: request the products
        self.productService.inputs.startProductRequest(with: [ProductIdentifiers.consumbaleTestOne])
        
        //product service output: receive the response
        self.productService.outputs.receivedReponse
            .subscribe(onNext: { [weak self] response in
                
                guard let strongSelf = self,
                          let response = response,
                          let elements = response.last?.elements,
                          let product = elements.last as? SKProduct else {return}
                
                //payment service input: buy request
                strongSelf.paymentService.inputs.buy(product)
                
            }).disposed(by: self.disposeBag)

        //payment service output: receive the transactions
        self.paymentService.outputs.purchasedTransactions
            .subscribe(onNext: { purchasedTransactions in
                purchasedTransactions?.forEach({ (transaction) in
                    switch transaction.payment.productIdentifier {
                    case ProductIdentifiers.consumbaleTestOne:
                        // Handling
                        break
                    default:
                        //
                        break
                    }
                })
            }).disposed(by: self.disposeBag)
    }
    
    /// Extends viewcontroller to conform to  store services delegates with .delegative service ).
    /// Overrided methods of ProductService & PaymentService Delegate's
    /// NOTE: This methods will called if ServiceType is delegative
    override func _paymentServiceRestoreDidSucceed() {
        debugPrint("_paymentServiceRestoreDidSucceed")
    }

    override func _paymentServiceTransactionDidFinished(state: SKPaymentTransactionState) {
        debugPrint(state)
    }

    override func _paymentServiceDidReceiveMessage(_ message: String) {
        debugPrint(message)
    }

    override func _productServiceDidReceiveResponse(_ response: [Section]) {
        debugPrint(response)
//        guard let elements = response.last?.elements else {return}
//        guard let product = elements.last as? SKProduct else {return}
//        self.paymentService.buy(product)
    }

    override func _productServiceDidReceiveMessage(_ message: String) {
        debugPrint(message)
    }
}
