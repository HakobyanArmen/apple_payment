//
//  HasStorable.swift
//  ApplePayment
//
//  Created by Armen Hakobyan on 10/11/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

protocol HasStorable {
    var paymentService: PaymentServiceType { get }
    var productService: ProductServiceType { get }
}

extension HasStorable {
    /// Payment service for dependency instance factories
    var paymentService: PaymentServiceType {
            return Dependency.shared.resolver.resolve(PaymentServiceType.self)!
    }
    
    /// Product service for dependency instance factories
    var productService: ProductServiceType {
            return Dependency.shared.resolver.resolve(ProductServiceType.self)!
    }
}
