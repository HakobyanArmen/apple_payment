//
//  StorableViewController.swift
//  ApplePayment
//
//  Created by Armen Hakobyan on 30/10/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import UIKit
import StoreKit

/// Used to extends view controller to conform to  Storable sevicies delegates.
///
/// - delegative:  conform to  ProductService and PaymentService delegates.
///
/// - notDelegative: not conform to  ProductService and PaymentService delegates.

enum ServiceType {
    case delegative
    case notDelegative
}

class StorableViewController: UIViewController, HasDisposeBag, HasStorable {
    
    var serviceType: ServiceType {
        return .delegative
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if serviceType == .delegative {
            productService.manager.delegate = self
            paymentService.manager.delegate = self
        }
    }
    
    /// Delegate to childe view controllers by this methods.
    // Product Output
    public func _productServiceDidReceiveResponse(_ response: [Section]) {}
    public func _productServiceDidReceiveMessage(_ message: String) {}
    
    // Payment Output
    public func _paymentServiceRestoreDidSucceed() {}
    public func _paymentServiceTransactionDidFinished(state: SKPaymentTransactionState) {}
    public func _paymentServiceDidReceiveMessage(_ message: String) {}
}

/// Extends viewcontroller to conform to  ProductServiceDelegate.
/// Works if ServiceType is delegative.
extension StorableViewController: ProductServiceDelegate {
    func productServiceDidReceiveResponse(_ response: [Section]) {
        _productServiceDidReceiveResponse(response)
    }
    
    func productServiceDidReceiveMessage(_ message: String) {
        _productServiceDidReceiveMessage(message)
    }
}

/// Extends viewcontroller to conform to  PaymentServiceDelegate.
/// Works if ServiceType is delegative.
extension StorableViewController:  PaymentServiceDelegate{
    func paymentServiceRestoreDidSucceed() {
        _paymentServiceRestoreDidSucceed()
    }
    
    func paymentServiceTransactionDidFinished(state: SKPaymentTransactionState) {
        _paymentServiceTransactionDidFinished(state: state)
    }
    
    func paymentServiceDidReceiveMessage(_ message: String) {
        _paymentServiceDidReceiveMessage(message)
    }
}
