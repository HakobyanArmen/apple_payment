//
//  ProductService.swift
//  ApplePayment
//
//  Created by Armen Hakobyan on 30/10/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import RxSwift
import StoreKit

protocol ProductServiceTypeInputs {
    /// Starts the product request with the specified identifiers.
    func startProductRequest(with identifiers: [String])
    
    /// - returns: Existing product's title matching the specified product identifier.
    func title(matchingIdentifier identifier: String) -> String?
    
    /// - returns: Existing product's title associated with the specified payment transaction.
    func title(matchingPaymentTransaction transaction: SKPaymentTransaction) -> String
}

protocol ProductServiceTypeOutputs {
    var receivedReponse: BehaviorSubject<[Section]?> { get }
    var receivedMessage: BehaviorSubject<String?> { get }
}

protocol ProductServiceType {
    var manager: ProductService { get }
    var inputs: ProductServiceTypeInputs { get }
    var outputs: ProductServiceTypeOutputs { get }
}

protocol ProductServiceDelegate: class {
    /// Provides the delegate with the App Store's response.
    func productServiceDidReceiveResponse(_ response: [Section])

    /// Provides the delegate with the error encountered during the product request.
    func productServiceDidReceiveMessage(_ message: String)
}

extension ProductServiceDelegate {
    func productServiceDidReceiveResponse(_ response: [Section]){}
    func productServiceDidReceiveMessage(_ message: String){}
}

final class ProductService: NSObject, ProductServiceType, ProductServiceTypeInputs, ProductServiceTypeOutputs, ProductServiceDelegate {
            
    var receivedReponse: BehaviorSubject<[Section]?> =  BehaviorSubject(value: [])    
    var receivedMessage: BehaviorSubject<String?> = BehaviorSubject(value: nil)
    
    var manager: ProductService {return self}
    var inputs: ProductServiceTypeInputs {return self}
    var outputs: ProductServiceTypeOutputs {return self}

    // MARK: - Properties

    /// Keeps track of all valid products. These products are available for sale in the App Store.
    fileprivate var availableProducts = [SKProduct]()

    /// Keeps track of all invalid product identifiers.
    fileprivate var invalidProductIdentifiers = [String]()

    /// Keeps a strong reference to the product request.
    fileprivate var productRequest: SKProductsRequest!

    /// Keeps track of all valid products (these products are available for sale in the App Store) and of all invalid product identifiers.
    fileprivate var storeResponse = [Section]()
    
    weak var delegate: ProductServiceDelegate?

    // MARK: - Request Product Information

    /// Starts the product request with the specified identifiers.
    func startProductRequest(with identifiers: [String]) {
        fetchProducts(matchingIdentifiers: identifiers)
    }

    /// Fetches information about your products from the App Store.
    /// - Tag: FetchProductInformation
    fileprivate func fetchProducts(matchingIdentifiers identifiers: [String]) {
        // Create a set for the product identifiers.
        let productIdentifiers = Set(identifiers)

        // Initialize the product request with the above identifiers.
        productRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productRequest.delegate = self

        // Send the request to the App Store.
        productRequest.start()
    }

    // MARK: - Inputs
    
    /// - returns: Existing product's title matching the specified product identifier.
    func title(matchingIdentifier identifier: String) -> String? {
        var title: String?
        guard !availableProducts.isEmpty else { return nil }

        // Search availableProducts for a product whose productIdentifier property matches identifier. Return its localized title when found.
        let result = availableProducts.filter({ (product: SKProduct) in product.productIdentifier == identifier })

        if !result.isEmpty {
            title = result.first!.localizedTitle
        }
        return title
    }

    /// - returns: Existing product's title associated with the specified payment transaction.
    func title(matchingPaymentTransaction transaction: SKPaymentTransaction) -> String {
        let title = self.title(matchingIdentifier: transaction.payment.productIdentifier)
        return title ?? transaction.payment.productIdentifier
    }
}

// MARK: - SKProductsRequestDelegate

/// Extends StoreManager to conform to SKProductsRequestDelegate.
extension ProductService: SKProductsRequestDelegate {
    /// Used to get the App Store's response to your request and notify your observer.
    /// - Tag: ProductRequest
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if !storeResponse.isEmpty {
            storeResponse.removeAll()
        }

        // products contains products whose identifiers have been recognized by the App Store. As such, they can be purchased.
        if !response.products.isEmpty {
            availableProducts = response.products
            storeResponse.append(Section(type: .availableProducts, elements: availableProducts))
        }

        // invalidProductIdentifiers contains all product identifiers not recognized by the App Store.
        if !response.invalidProductIdentifiers.isEmpty {
            invalidProductIdentifiers = response.invalidProductIdentifiers
            storeResponse.append(Section(type: .invalidProductIdentifiers, elements: invalidProductIdentifiers))
        }

        if !storeResponse.isEmpty {
            DispatchQueue.main.async {
                self.delegate?.productServiceDidReceiveResponse(self.storeResponse)                
                self.receivedReponse.onNext(self.storeResponse)
            }
        }
    }
}

// MARK: - SKRequestDelegate

/// Extends ProductService to conform to SKRequestDelegate.
extension ProductService: SKRequestDelegate {
    /// Called when the product request failed.
    func request(_ request: SKRequest, didFailWithError error: Error) {
        DispatchQueue.main.async {
            self.delegate?.productServiceDidReceiveMessage(error.localizedDescription)
            self.receivedMessage.onNext(error.localizedDescription)
        }
    }
}
