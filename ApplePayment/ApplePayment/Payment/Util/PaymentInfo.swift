//
//  PaymentInfo.swift
//  ApplePayment
//
//  Created by Armen Hakobyan on 11/11/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import Foundation

class PaymentInfo {
    static func receiptData() -> Data? {
        if let appStoreReceiptUrl = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptUrl.path) {
            do {
                let rawReceiptData = try Data(contentsOf: appStoreReceiptUrl)
                let receiptData = rawReceiptData.base64EncodedData(options: Data.Base64EncodingOptions.init())
                return receiptData
            } catch {
                return nil
            }
        }
        return nil
    }
}
