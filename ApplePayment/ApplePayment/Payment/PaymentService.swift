//
//  PaymentService.swift
//  ApplePayment
//
//  Created by Armen Hakobyan on 30/10/2019.
//  Copyright © 2019 Armen Hakobyan. All rights reserved.
//

import RxSwift
import StoreKit

protocol PaymentServiceTypeInput {
    /// Create and add a payment request to the payment queue.
    func buy(_ product: SKProduct)
    
    /// Restores all previously completed purchases.
    func restore()
}

protocol PaymentServiceOutput {
    var purchasedTransactions: BehaviorSubject<[SKPaymentTransaction]?> { get }
    var restoredTransactions: BehaviorSubject<[SKPaymentTransaction]?> { get }

    var finishedTransaction: BehaviorSubject<SKPaymentTransactionState?> { get }
    var receivedMessage: BehaviorSubject<String?> { get }
    var succeedRestore: BehaviorSubject<Bool?> { get }
}

protocol PaymentServiceType {
    var manager: PaymentService { get }
    var inputs: PaymentServiceTypeInput { get }
    var outputs: PaymentServiceOutput { get }
}

protocol PaymentServiceDelegate: class {
    /// Tells the delegate that the restore operation was successful.
    func paymentServiceRestoreDidSucceed()
    
    /// Provides the delegate with transaction state.
    func paymentServiceTransactionDidFinished(state: SKPaymentTransactionState)

    /// Provides the delegate with messages.
    func paymentServiceDidReceiveMessage(_ message: String)
}

final class PaymentService: NSObject, PaymentServiceType, PaymentServiceOutput, PaymentServiceTypeInput {
    var restoredTransactions: BehaviorSubject<[SKPaymentTransaction]?> = BehaviorSubject(value: [])
    
    var finishedTransaction: BehaviorSubject<SKPaymentTransactionState?> = BehaviorSubject(value: nil)
    
    var receivedMessage: BehaviorSubject<String?> = BehaviorSubject(value: nil)
    
    var succeedRestore: BehaviorSubject<Bool?> = BehaviorSubject(value: nil)
    
    var purchasedTransactions: BehaviorSubject<[SKPaymentTransaction]?> = BehaviorSubject(value: [])
    
    var manager: PaymentService {return self}
    var inputs: PaymentServiceTypeInput {return self}
    var outputs: PaymentServiceOutput {return self}
    
    
    deinit {
        // Remove Any Instance Delegates
        SKPaymentQueue.default().remove(self)
    }
    // MARK: - Properties

    /**
    Indicates whether the user is allowed to make payments.
    - returns: true if the user is allowed to make payments and false, otherwise. Tell StoreManager to query the App Store when the user is
    allowed to make payments and there are product identifiers to be queried.
    */
    var isAuthorizedForPayments: Bool {
        return SKPaymentQueue.canMakePayments()
    }

    /// Keeps track of all purchases.
    var purchased = [SKPaymentTransaction]()

    /// Keeps track of all restored purchases.
    var restored = [SKPaymentTransaction]()

    /// Indicates whether there are restorable purchases.
    fileprivate var hasRestorablePurchases = false

    weak var delegate: PaymentServiceDelegate?

    // MARK: - Inputs: Submit Payment Request

    /// Create and add a payment request to the payment queue.
    func buy(_ product: SKProduct) {
        let payment = SKMutablePayment(product: product)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
    }

    // MARK: - Restore All Restorable Purchases

    /// Restores all previously completed purchases.
    func restore() {
        if !restored.isEmpty {
            restored.removeAll()
        }
        SKPaymentQueue.default().restoreCompletedTransactions()
    }

    // MARK: - Handle Payment Transactions

    /// Handles successful purchase transactions.
    fileprivate func handlePurchased(_ transaction: SKPaymentTransaction) {
        purchased.append(transaction)
        purchasedTransactions.onNext(purchased)
        print("\(Messages.deliverContent) \(transaction.payment.productIdentifier).")

        // Finish the successful transaction.
        SKPaymentQueue.default().finishTransaction(transaction)
        
        // TODO: Check subscription: server-to-server validation
        /*
        guard let receiptData = PaymentInfo.receiptData() else {
            SKPaymentQueue.default().finishTransaction(transaction)
            return
        }
        // Send up receiptData to server
        userService.processTransaction(receiptData) { isValid in
            if isValid {
                SKPaymentQueue.default().finishTransaction(transaction)
            }
        }
         */
    }

    /// Handles failed purchase transactions.
    fileprivate func handleFailed(_ transaction: SKPaymentTransaction) {
        var message = "\(Messages.purchaseOf) \(transaction.payment.productIdentifier) \(Messages.failed)"

        if let error = transaction.error {
            message += "\n\(Messages.error) \(error.localizedDescription)"
            print("\(Messages.error) \(error.localizedDescription)")
        }

        // Do not send any notifications when the user cancels the purchase.
        if (transaction.error as? SKError)?.code != .paymentCancelled {
            DispatchQueue.main.async {
                self.delegate?.paymentServiceDidReceiveMessage(message)
                self.receivedMessage.onNext(message)
            }
        }
        // Finish the failed transaction.
        SKPaymentQueue.default().finishTransaction(transaction)
    }

    /// Handles restored purchase transactions.
    fileprivate func handleRestored(_ transaction: SKPaymentTransaction) {
        hasRestorablePurchases = true
        restored.append(transaction)
        print("\(Messages.restoreContent) \(transaction.payment.productIdentifier).")

        DispatchQueue.main.async {
            self.delegate?.paymentServiceRestoreDidSucceed()
            self.restoredTransactions.onNext(self.restored)
            self.succeedRestore.onNext(true)
        }
        // Finishes the restored transaction.
        SKPaymentQueue.default().finishTransaction(transaction)
    }
}

// MARK: - SKPaymentTransactionObserver

/// Extends PaymentService to conform to SKPaymentTransactionObserver.
extension PaymentService: SKPaymentTransactionObserver {
    /// Called when there are transactions in the payment queue.
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing: break
            // Do not block your UI. Allow the user to continue using your app.
            case .deferred: print(Messages.deferred)
            // The purchase was successful.
            case .purchased: handlePurchased(transaction)
            // The transaction failed.
            case .failed: handleFailed(transaction)
            // There are restored products.
            case .restored: handleRestored(transaction)
            @unknown default: fatalError("\(Messages.unknownDefault)")
            }
            
            self.delegate?.paymentServiceTransactionDidFinished(state: transaction.transactionState)
            self.finishedTransaction.onNext(transaction.transactionState)
        }
    }

    /// Logs all transactions that have been removed from the payment queue.
    func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            print ("\(transaction.payment.productIdentifier) \(Messages.removed)")
        }
    }

    /// Called when an error occur while restoring purchases. Notify the user about the error.
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        if let error = error as? SKError, error.code != .paymentCancelled {
            DispatchQueue.main.async {
                self.delegate?.paymentServiceDidReceiveMessage(error.localizedDescription)
                self.receivedMessage.onNext(error.localizedDescription)
            }
        }
    }

    /// Called when all restorable transactions have been processed by the payment queue.
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print(Messages.restorable)

        if !hasRestorablePurchases {
            DispatchQueue.main.async {
                self.delegate?.paymentServiceDidReceiveMessage(Messages.noRestorablePurchases)
                self.receivedMessage.onNext(Messages.noRestorablePurchases)
            }
        }
    }
}
