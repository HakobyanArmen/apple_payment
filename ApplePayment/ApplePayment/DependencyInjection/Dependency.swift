//
//  Dependency.swift
//  IdealMatch
//
//  Created by Anatoli Petrosyants on 11/14/18.
//  Copyright © 2018 IdealMatch. All rights reserved.
//

import Foundation
import Swinject

final class Dependency {
    
    private var assembler: Assembler!
    
    var resolver: Resolver {
        return assembler.resolver
    }
    
    static var shared: Dependency = {
        return Dependency()
    }()
    
    private init() { }
    
    func initialize() {
        self.assembler = Assembler([
            ServicesAssembly(),
        ])
    }
}
