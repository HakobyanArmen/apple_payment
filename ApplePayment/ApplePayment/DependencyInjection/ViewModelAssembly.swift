//
//  ViewModelAssembly.swift
//  IdealMatch
//
//  Created by Anatoli Petrosyants on 11/14/18.
//  Copyright © 2018 IdealMatch. All rights reserved.
//

import Foundation
import Swinject

final class ViewModelAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(LoadingViewModelType.self) { r in
            let vm = LoadingViewModel(configurationsService: r.resolve(ConfigurationsServiceType.self)!,
                                      authService: r.resolve(AuthServiceType.self)!,
                                      userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(WelcomeViewModelType.self) { r in
            let vm = WelcomeViewModel(authService: r.resolve(AuthServiceType.self)!,
                                      userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(LoginViewModelType.self) { r in
            let vm = LoginViewModel(authService: r.resolve(AuthServiceType.self)!,
                                    userService: r.resolve(UserServiceType.self)!,
                                    validator: r.resolve(TextInputValidatorType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(PasswordRecoverViewModelType.self) { r in
            let vm = PasswordRecoverViewModel(authService: r.resolve(AuthServiceType.self)!,
                                              userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(RecoverWithEmailViewModelType.self) { r in
            let vm = RecoverWithEmailViewModel(authService: r.resolve(AuthServiceType.self)!,
                                               userService: r.resolve(UserServiceType.self)!,
                                               validator: r.resolve(TextInputValidatorType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(PasswordRecoverEmailVerificationViewModelType.self) { r in
            let vm = PasswordRecoverEmailVerificationViewModel(authService: r.resolve(AuthServiceType.self)!,
                                                               userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(RecoverWithPhoneViewModelType.self) { r in
            let vm = RecoverWithPhoneViewModel(authService: r.resolve(AuthServiceType.self)!,
                                               userService: r.resolve(UserServiceType.self)!,
                                               validator: r.resolve(TextInputValidatorType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(ResetPasswordViewModelType.self) { r in
            let vm = ResetPasswordViewModel(authService: r.resolve(AuthServiceType.self)!, validator: r.resolve(TextInputValidatorType.self)!)

            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(SignupEmailViewModelType.self) { r in
            let vm = SignupEmailViewModel(authService: r.resolve(AuthServiceType.self)!, validator: r.resolve(TextInputValidatorType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(EmailVerificationViewModelType.self) { r in
            let vm = EmailVerificationViewModel(authService: r.resolve(AuthServiceType.self)!,
                                                userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(AuthSuccessViewModelType.self) { r in
            let vm = AuthSuccessViewModel(userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(AgeRestrictionsViewModelType.self) { r in
            let vm = AgeRestrictionsViewModel()
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(AgeVerificationViewModelType.self) { r in
            let vm = AgeVerificationViewModel(validator: r.resolve(TextInputValidatorType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(AddNameViewModelType.self) { r in
            let vm = AddNameViewModel(validator: r.resolve(TextInputValidatorType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(AddGenderViewModelType.self) { r in
            let vm = AddGenderViewModel()
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(LookingForViewModelType.self) { r in
            let vm = LookingForViewModel()
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(AgeRangeViewModelType.self) { r in
            let vm = AgeRangeViewModel(userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(EnableLocationViewModelType.self) { r in
            let vm = EnableLocationViewModel()
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(ProfilePhotoViewModelType.self) { r in
            let vm = ProfilePhotoViewModel()
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(UploadProfilePhotoViewModelType.self) { r in
            let vm = UploadProfilePhotoViewModel(userService: r.resolve(UserServiceType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
        
        container.register(RootViewModelType.self) { r in
            let vm = RootViewModel(storage: r.resolve(StorageContextType.self)!)
            vm.coordinator = r.resolve(CoordinatorType.self)
            return vm
        }
    }
}
