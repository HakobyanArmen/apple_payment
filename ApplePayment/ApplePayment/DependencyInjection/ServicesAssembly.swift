//
//  ServicesAssembly.swift
//  IdealMatch
//
//  Created by Anatoli Petrosyants on 11/14/18.
//  Copyright © 2018 IdealMatch. All rights reserved.
//

import Foundation
import Swinject

final class ServicesAssembly: Assembly {
    func assemble(container: Container) {
        // sourcery:inline:Services.AutoInject
        container.register(ProductServiceType.self) { r in
            return ProductService()
        }
        
        container.register(PaymentServiceType.self) { r in
            return PaymentService()
        }
    }
}
